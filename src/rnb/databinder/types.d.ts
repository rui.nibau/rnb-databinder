
interface DataBinderUpdate {
    /** Data name */
    prop: string;
    /** New data value*/
    value: any;
    /** Old data value */
    oldValue?: any;
}

interface DataBinderOptions {
    /**
     * Basepath for media when source is relative
     */
    mediaBasepath?: string;
    /**
     * global date format
     */
    dateFormat?: Intl.DateTimeFormat;
}

interface BindingType {
    set: (element:HTMLElement, value: any) => void;
    get: (element:HTMLElement) => any;
}

interface BindingTypeList {
    [key: string]: BindingType
}

interface Binding {
    element: HTMLElement;
    type: string;
    prop: string;
    editable?: boolean;
    previous?: any;
    formatter?: any;
}

type DataBinderUpdateEvent = CustomEvent<DataBinderUpdate>;

type ConditionDef = {
    prop: string;
    not: boolean;
    elems: HTMLElement[]
}

type ConditionTest = {
    if?: string;
    ifNot?: string;
}

type ConditionResolver = (data:Object<string, any>) => boolean;

type LoopDef = {
    prop: string;
    template: HTMLElement | DocumentFragment;
    target: HTMLElement
    bindings: Binding[]
}