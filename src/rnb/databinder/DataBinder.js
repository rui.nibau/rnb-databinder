/**
 * @param {Function} fn 
 * @param {number} delay 
 * @returns {Function}
 */
const debounce = (fn, delay) => {
    let timeoutId;
    return function (...args) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => fn.apply(fn, args), delay);
    };
};

/**
 * Date formatter
 */
const dateFormater = new Intl.DateTimeFormat(undefined, {
    'day': '2-digit',
    'month': 'short',
    'year': 'numeric',
});

/**
 * 
 * @param {HTMLElement} element 
 * @returns {string}
 */
const getPropName = element => {
    if (element.dataset.prop) {
        return element.dataset.prop.toLowerCase();
    }
    if (element.hasAttribute('data-loop-key')) {
        return 'key';
    } else if (element.hasAttribute('data-loop-value')) {
        return 'value';
    }
    return null;
};

/**
 * @param {HTMLElement} element
 * @returns {Binding}
 */
const getBinding = element => {
    let prop = getPropName(element),
        nodeName = element.nodeName.toLowerCase(),
        type = 'string',
        editable = false;
    
    if (element instanceof HTMLAnchorElement) {
        type = 'link';
    } else if (element instanceof HTMLTimeElement) {
        type = 'date';
    } else if (nodeName === 'img' || nodeName === 'video') {
        type = 'media';
    } else if (element instanceof HTMLInputElement || element instanceof HTMLSelectElement) {
        editable = true;
        type = element.type === 'checkbox' || element.type === 'radio' ? 'boolean' : 'input';
    } else if (prop in bindingTypes) {
        type = prop;
    }

    return {element, type, prop, editable};
};

/**
 * 
 * @param {Binding} binding 
 * @param {any} value 
 * @param {DataBinderOptions} [options]
 */
const render = (binding, value, options = {}) => {
    // XXX Maybe it's better to clear element even if it's not displayed
    // if (binding.element.hidden) {
    //     return;
    // }
    binding.previous = bindingTypes[binding.type].get(binding.element);
    bindingTypes[binding.type].set(binding.element, value || null, options);
    if (binding.element.hasAttribute('data-prop-to')) {
        binding.element.setAttribute(binding.element.getAttribute('data-prop-to'), value || '');
    }
};

export const bindingTypes = {

    body: {
        /**
         * @param {HTMLElement} element 
         * @returns {string}
         */
        get(element) {
            return element.innerHTML;
        },
        /**
         * 
         * @param {HTMLElement} element 
         * @param {HTMLElement | string | DocumentFragment} body 
         */
        set(element, body) {
            element.innerHTML = '';
            if (body) {
                element.append(typeof body === 'string' ? body : body.cloneNode(true));
            }
        },
    },

    boolean: {
        /**
         * @param {HTMLInputElement} element 
         * @returns {boolean}
         */
        get(element) {
            return element.checked;
        },
        /**
         * @param {HTMLInputElement} element 
         * @param {boolean} value 
         */
        set(element, value) {
            element.checked = value;
        },
    },

    date: {
        /**
         * @param {HTMLTimeElement} element
         * @returns {Date}
         */
        get(element) {
            if (element.dateTime) {
                return new Date(element.dateTime);
            }
            return null;
        },
        /**
         * @param {HTMLTimeElement} element
         * @param {string} date 
         * @param {DataBinderOptions} [options]
         */
        set(element, date, options) {
            if (date) {
                element.textContent = ((options && options.dateFormat) ? options.dateFormat : dateFormater)
                    .format(new Date(date));
                element.dateTime = date;
            } else {
                element.textContent = '';
                element.dateTime = '';
            }
        },
    },
    
    media: {
        /**
         * @param {HTMLImageElement} element 
         * @returns {string}
         */
        get(element) {
            return element.src;
        },
        /**
         * @param {HTMLImageElement} element 
         * @param {string} src 
         * @param {DataBinderOptions} [options]
         */
        set(element, src, options) {
            if (!element.dataset.lock || !element.dataset.lock.includes('src')) {
                if (options && options.mediaBasepath && src && !src.startsWith('http') && !src.startsWith('/')) {
                    const sep = options.mediaBasepath.endsWith('/') ? '' : '/';
                    src = `${options.mediaBasepath}${sep}${src}`;
                }
                element.src = src || '';
            }
        },
    },

    input: {
        /**
         * @param {HTMLInputElement} element 
         * @returns {string} value 
         */
        get(element) {
            return element.value;
        },
        /**
         * @param {HTMLInputElement} element 
         * @param {string} value 
         */
        set(element, value) {
            element.value = value || '';
        },
    },
    
    link: {
        /**
         * 
         * @param {HTMLAnchorElement} element 
         * @param {string | object} data 
         */
        get(element) {
            return {url:element.href, text:element.textContent};
        },
        /**
         * 
         * @param {HTMLAnchorElement} element 
         * @param {string|{url:string, text?:string, content?:string}} data 
         */
        set(element, data) {
            const lockHref = element.dataset.lock && element.dataset.lock.includes('href');
            if (data) {
                if (typeof data === 'string') {
                    if (!lockHref) {
                        element.href = data;
                        if (!data) {
                            element.removeAttribute('href');
                        }
                    }
                    if (!element.textContent) {
                        element.textContent = data;
                    }
                } else {
                    if (!lockHref) {
                        element.href = data.url || '';
                        if (!data.url) {
                            element.removeAttribute('href');
                        }    
                    }
                    if (data.content || data.text) {
                        element.textContent = data.content || data.text;
                    }
                }
            } else {
                if (!lockHref) {
                    element.href = '';
                    element.removeAttribute('href');
                }
                element.textContent = '';
            }
        },

    },

    string: {
        /**
         * @param {HTMLElement} element
         * @returns {string} str 
         */
        get(element) {
            if (element.dataset.value) {
                return element.dataset.value;
            }
            return element.textContent;
        },
        /**
         * @param {HTMLElement} element
         * @param {string} str 
         */
        set(element, str) {
            element.textContent = str || '';
        },
    },
};

export const EVENT_DATA_BINDER_UPDATED = 'databinder-updated';

/**
 * Simple class to bind data to HTML
 * 
 * @fires DataBinderUpdateEvent When data is updated by user
 */
export default class DataBinder extends EventTarget {

    /**@type {HTMLElement}*/
    parentElement = null;
    
    /**@type {Binding[]}*/
    bindings = [];

    /**@type {ConditionDef[]}*/
    conditions = [];
    
    /**@type {Object.<string, ConditionResolver>}*/
    conditionRes = {};
    
    /**@type {LoopDef[]}*/
    loops = [];

    /**@type {Map<HTMLElement, ConditionTest>} */
    conditionsMap = new Map();

    /**@type {Object.<string, any>} */
    edits = {};

    /**
     * @type {DataBinderOptions}
     * @private
     */
    options = {};

    /**
     * @param {HTMLElement} [parentElement] Where the data needs to be rendered (body by default)
     * @param {DataBinderOptions} [options = null]
     * @param {object} [data] Initial data
     */
    constructor (parentElement = globalThis.document.body, options = null, data) {
        super();
        this.parentElement = parentElement;
        this.options = options;

        let editable = false;

        // bindings
        this.parentElement.querySelectorAll('[data-prop]').forEach(element => {
            const binding = getBinding(/**@type {HTMLElement}*/(element));
            this.bindings.push(binding);
            if (binding.editable) {
                editable = true;
            }
        });

        // Conditions
        this.parentElement.querySelectorAll('[data-if],[data-if-not]').forEach((/**@type {HTMLElement}*/element) => {
            const ifProp = element.dataset.if || null,
                ifNotProp = element.dataset.ifNot || null;
            if (ifProp || ifNotProp) {
                this.conditionsMap.set(element, {if: ifProp, ifNot: ifNotProp});
            }
        });
        
        // Loops
        // TODO Loop based on templates
        this.parentElement.querySelectorAll('[data-loop]').forEach((/**@type {HTMLElement}*/el) => {
            /**@type {LoopDef} */
            const loop = {bindings: [], prop: '', target: null, template: null};
            loop.template = document.createDocumentFragment();
            loop.template.append(...el.children);
            loop.prop = el.dataset.loop;
            loop.target = el;
            loop.template.querySelectorAll('[data-loop-key],[data-loop-value]')
                .forEach((/**@type {HTMLElement}*/c) => {
                    loop.bindings.push(getBinding(c));
                });
            this.loops.push(loop);
        });
        
        if (data) {
            this.update(data);
        }
        if (editable) {
            /**
             * @type {Function}
             * @param {Binding} binding
             */
            this.emitUpdate = debounce((/**@type {Binding}*/binding) => {
                const value = bindingTypes[binding.type].get(binding.element);
                this.dispatchEvent(new CustomEvent(EVENT_DATA_BINDER_UPDATED, {
                    /**@type {DataBinderUpdate} */
                    detail: {prop: binding.prop, value},
                }));
                binding.previous = value;
            }, 450);

            this.parentElement.addEventListener('input', e => {
                const element = /**@type {HTMLElement}*/(e.target),
                    prop = element.dataset.prop.toLowerCase(),
                    binding = this.getBinding(element);
                if (binding) {
                    this.emitUpdate(binding);
                }
            });
        }
    }

    /**
     * Get binding definition for a given element
     * 
     * @param {Element} element 
     * @returns {Binding}
     */
    getBinding(element) {
        return this.bindings.find(b => b.element === element);
    }

    /**
     * Get bindings for a given property
     * 
     * @param {string} prop 
     * @returns {Binding[]}
     */
    getBindings(prop) {
        return this.bindings.filter(binding => binding.prop === prop);
    }

    /**
     * 
     * @param {Object<string, any>} data 
     * @param {DataBinderOptions} [options]
     */
    update(data, options) {
        
        this.parentElement.classList.add('updating');

        if (options) {
            this.options = options;
        }

        // process conditions
        for (const [element, condition] of this.conditionsMap.entries()) {
            let display = false;
            if (condition.if) {
                display = condition.if in data;
                if (display && condition.ifNot && condition.ifNot in data) {
                    display = false;
                }
            } else if (condition.ifNot && !(condition.ifNot in data)) {
                display = true;
            }
            element.hidden = !display;
        }

        // process bindings
        this.bindings.forEach(binding => render(binding, data[binding.prop], this.options));

        // process loops
        this.loops.forEach(loop => {
            loop.target.innerHTML = '';
            if (loop.prop in data) {
                const values = data[loop.prop];
                if (typeof values === 'object' || Array.isArray(values)) {
                    Object.entries(values).forEach(([key, value]) => {
                        const d = {value, key};
                        loop.bindings.forEach(binding => {
                            render(binding, d[binding.prop], this.options);
                            // bindingTypes[binding.type].set(binding.element, d[binding.prop],
                            //     binding.formatter);
                        });
                        loop.target.append(loop.template.cloneNode(true));
                    });
                }
            }
        });
        
        globalThis.setTimeout(() => this.parentElement.classList.remove('updating'));
    }

    /**
     * Get value of a given binded property
     * 
     * @param {string} prop Prop name
     * @returns {any} Data value
     */
    getValue(prop) {
        // FIXME Find a better way
        const bindings = this.getBindings(prop);
        if (bindings.length > 0) {
            const binding = bindings[0];
            return bindingTypes[binding.type].get(binding.element);
        }
        return null;
    }

    /**
     * Set value of a given binded property.
     * 
     * @param {string} prop Data name
     * @param {any} value Data value
     */
    setValue(prop, value) {
        this.getBindings(prop).forEach(binding => render(binding, value));
        this.dispatchEvent(new CustomEvent(EVENT_DATA_BINDER_UPDATED, {
            /**@type {DataBinderUpdate} */
            detail: {prop, value},
        }));
    }

    // /**
    //  * Set formatter for a specific field. Not all fields can be formatted.
    //  * 
    //  * * Fields containing a date can be formatted with a Intl.DateTimeFormat object.
    //  * 
    //  * @param {String} name Data name
    //  * @param {any} formatter Formatter object
    //  */
    // setFormatter (name, formatter) {
    //     if (name in this.bindings) {
    //         this.bindings[name].formatter = formatter;
    //     } else {
    //         // XXX Create binding ?
    //         console.log(`No binding for data ${name}`);
    //     }
    // }

    /**
     * 
     * @param {string} name 
     * @param {ConditionResolver} resolver 
     */
    setConditionResolver(name, resolver) {
        this.conditionRes[name] = resolver;
    }
}
