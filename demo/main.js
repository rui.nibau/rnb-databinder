import DataBinder, {EVENT_DATA_BINDER_UPDATED} from './js/rnb/databinder/DataBinder.js';

// create data binding
const dataBinder = new DataBinder(document.getElementById('form'), {
    country: 'France',
    greetings: 'Bonjour le monde'
});

// Listen to updates from ui and update viewer
dataBinder.addEventListener(EVENT_DATA_BINDER_UPDATED, (e) => {
    const update = /**@type {DataBinderUpdateEvent}*/(e).detail;
    const id = `view-${update.prop}`;
    document.getElementById(id).textContent = update.value;
});

// Listen to events in setter and update data
document.getElementById('setter').addEventListener('input', (e) => {
    const el = e.target,
        key = el.dataset.key,
        value = el.value;
    dataBinder.setValue(key, value);
});
