---
title:      rnb-dataBinder
date:       2020-01-13
updated:    2024-01-31
cats:       [informatique]
tags:       [publication]
techs:      [html, javascript]
source:     https://framagit.org/rui.nibau/rnb-databinder
issues:     https://framagit.org/rui.nibau/rnb-databinder/-/issues
status:     in-process
version:    0.1.1
itemtype:   WebApplication
pagesAsSnippets: true
intro: Un simple exercice de style ou ??POC|Proof Of Concept?? pour illustrer une manière parmi d'autre de lier une interface graphique HTML et des données sans sortir l'artillerie lourde.
---

## Présentation

C'est un des arguments foireux que j'entend le plus souvent quand on essaye de justifier l'usage d'usines à gaz que sont React ou Angular : ces frameworks facilitent la liaison entre un jeu de données et des éléments HTML pour visualiser / éditer ces données. Foireux dans le sens ou (1) des solutions de liaison ou « databinding » existaient bien avant l'arrivée de ces frameworks, (2) que leur manière de faire n'est pas forcément très pertinente et (3) qu'on peut très bien faire sans avec seulement quelques dizaines de lignes de code.

``rnb-databinder`` est un exemple écrit rapidement répondant à des besoins simples de databinding. Il n'a pas pour prétention de répondre à tous les besoins ni d'être fini, il illustre simplement le fait qu'on peut très facilement lier une interface graphique web et des données, mettre à jour l'interface quand ces données sont modifiées et inversement, le tout sans avoir besoin d'usines à gaz qui veulent tout faire.

## Démonstration

[°°||style=height: 30rem;°°iframe:](/lab/rnb-databinder/demo/index.html)

## Utilisation

----
°°stx-html°°
    <div id="mydata">
        <p><input type="text" name="data1" data-prop="data1"></p>
        <p><input type="checkbox" name="data2" data-prop="data2"></p>
    </div>
    <script type="module" src="main.js"></script>
----
index.html

----
°°stx-js°°
    import DataBinder, {EVENT_DATA_BINDER_UPDATED} 
            from 'rnb/databinder/DataBinder.js';
    
    // create binder
    const binding = new DataBinder(document.getElementById('mydata'), {
        data1: 'Hello World',
        data2: true
    });
    
    // Listen to graphical changes
    binding.addEventListener(EVENT_DATA_BINDER_UPDATED, (e) => {
        // Name of the property that has been modified
        const prop = e.detail.prop;
        // New value of the property
        const value = e.detail.value;
        // Do whatever you want we new data
        // ...
    });
    
    // update graphical interface
    binding.set('data2', false);
    binding.set('data1', 'Bonjour le monde');
----
main.js

°°|conditionnal°°
## Conditionnal

Status: work in progress...

### Simple condition

The attribute ``data-if`` allows to display a element only if data with that name is defined.

°°stx-html°°
    <p data-if="foo" data-prop="foo"></p>

Example : ``cover`` image will only be displayed if ``cover`` exists :

°°stx-html°°
    <p data-if="cover">
        <img data-prop="cover" alt="product cover">
    </p>

### Negative condition

The attribute ``data-if-not`` do the opposite : elements will be displayed if the data is not defined.

°°stx-html°°
    <p data-if-not="value">Value is not defined</p>


### User defined condition

°°todo°°Todo...

°°|loop°°
## Loops

°°todo°°Status: work in progress...

## Historique

..include::./changelog.md

## Licence

..include::./licence.md

