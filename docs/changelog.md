---
title:      Historique
date:       2020-01-13
updated:    2021-10-11
---

°°changelog°°
2021-10-11 (1.1.1):
    • upd: Version évoluée pour pouvoir être utilisée dans [rnb-pub](/projets/rnb-pub).
2020-01-13 (1.0.0):
    • add: Première version simple (édition) adaptant les pratiques des années précédentes.

