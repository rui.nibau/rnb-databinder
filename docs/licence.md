---
title:      Historique
date:       2020-01-13
updated:    2023-09-18
---

rnb-databinder - Simple javascript DataBinding

Copyright (C) 2020-2023 Rui Nibau, licensed using GPL V2.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

